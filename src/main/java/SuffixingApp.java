import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SuffixingApp {
    private static final Logger logger = Logger.getLogger(SuffixingApp.class.getName());

    public static void main(String[] args) {
        if (args.length != 1) {
            logger.severe("Config file path is required as the only argument");
            return;
        }

        String configFilePath = args[0];
        Properties properties = new Properties();

        try (FileInputStream fis = new FileInputStream(configFilePath)) {
            properties.load(fis);
        } catch (IOException e) {
            logger.severe("Unable to read config file: " + configFilePath);
            return;
        }

        String mode = properties.getProperty("mode");
        String suffix = properties.getProperty("suffix");
        String files = properties.getProperty("files");

        if (mode == null || suffix == null || files == null) {
            if (mode == null) {
                logger.severe("Mode is not configured");
            }
            if (suffix == null) {
                logger.severe("No suffix is configured");
            }
            if (files == null) {
                logger.warning("No files are configured to be copied/moved");
            }
            return;
        }

        if (!mode.equalsIgnoreCase("copy") && !mode.equalsIgnoreCase("move")) {
            logger.severe("Mode is not recognized: " + mode);
            return;
        }

        String[] filePaths = files.split(":");
        for (String filePath : filePaths) {
            Path path = Paths.get(filePath);
            if (Files.notExists(path)) {
                logger.severe("No such file: " + path.toString().replace("\\", "/"));
                continue;
            }

            String newFilePath = getNewFileName(path.toString(), suffix);
            Path newPath = Paths.get(newFilePath);

            try {
                if (mode.equalsIgnoreCase("copy")) {
                    Files.copy(path, newPath);
                    logger.info(path.toString().replace("\\", "/") + " -> " + newPath.toString().replace("\\", "/"));
                } else if (mode.equalsIgnoreCase("move")) {
                    Files.move(path, newPath);
                    logger.info(path.toString().replace("\\", "/") + " => " + newPath.toString().replace("\\", "/"));
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Error processing file: " + path.toString().replace("\\", "/"), e);
            }
        }
    }

    private static String getNewFileName(String originalFileName, String suffix) {
        int dotIndex = originalFileName.lastIndexOf(".");
        if (dotIndex == -1) {
            return originalFileName + suffix;
        } else {
            return originalFileName.substring(0, dotIndex) + suffix + originalFileName.substring(dotIndex);
        }
    }
}
